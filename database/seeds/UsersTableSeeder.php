<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Nikola Jovanovic',
            'email' => 'nikola@gmail.com',
            'password' => bcrypt('123'),
        ]);
        DB::table('users')->insert([
            'name' => 'Aleksandar Vukasinovic',
            'email' => 'aleksandar@gmail.com',
            'password' => bcrypt('123'),
        ]);
    }
}
