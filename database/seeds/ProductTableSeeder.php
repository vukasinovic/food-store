<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Burger King 1',
            'description' => 'Lorem ipsum blabla awdawdawdwa',
            'weight' => 300,
            'price' => 250,
        ]);
        DB::table('products')->insert([
            'name' => 'Burger King 2',
            'description' => 'Lorem ipsum blabla awdawdawdwa',
            'weight' => 350,
            'price' => 300,
        ]);
        DB::table('products')->insert([
            'name' => 'Velika Pljeskavica',
            'description' => 'Lorem ipsum blabla awdawdawdwa',
            'weight' => 200,
            'price' => 170,
        ]);
        DB::table('products')->insert([
            'name' => 'Mala Pljeskavica',
            'description' => 'Lorem ipsum blabla awdawdawdwa',
            'weight' => 100,
            'price' => 80,
        ]);
    }
}
