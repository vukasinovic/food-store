<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <title>Test</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/angular/angular.min.js"></script>
        <script src="bower_components/angular-route/angular-route.min.js"></script>
        <script src="bower_components/angular-local-storage/dist/angular-local-storage.min.js"></script>
        <script src="bower_components/angular-spinner/dist/angular-spinner.min.js"></script>
        <script src="js/app.js"></script>
        <script src="js/controller.js"></script>
        <script src="partials/home/home.js"></script>  
        <script src="js/service.js"></script>
        <script src="partials/login/login.js"></script>
        <script src="partials/single-view-menu/single-view-menu.js"></script>
        <script src="partials/admin-home-menu/admin-home-menu.js"></script>
        <script src="partials/admin-change/admin-change.js"></script>
        <script src="partials/add-new/add-new.js"></script>

        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css"></link>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="partials/home/home.css">
        <link rel="stylesheet" href="partials/login/login.css">
        <link rel="stylesheet" href="partials/single-view-menu/single-view-menu.css">
        <link rel="stylesheet" href="partials/admin-home-menu/admin-home-menu.css">
        <link rel="stylesheet" href="partials/add-new/add-new.css">
        <link rel="stylesheet" href="partials/admin-change/admin-change.css">
    </head>
    <body ng-app="myApp">
        <div class="container" ng-controller="MainController">
            <div class="row menu">
                <ul class="nav nav-pills">
                    <li role="presentation" class="active"><button class="btn btn-success" ng-click="goHome()">Home</a></li>
                    <li role="presentation">
                        <button class="btn btn-success" ng-click="goLogin()" ng-if="!isToken">Login</button>
                        <button class="btn btn-success" ng-click="goAdminPanel()" ng-if="isToken">Admin</button>
                        <button class="btn btn-success" ng-click="goLogout()" ng-if="isToken">Logout</button>
                    </li>
                </ul>
                <hr/>
            </div>
            <div ng-view></div>
        </div>
    </body>
</html>
