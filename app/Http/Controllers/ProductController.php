<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use Tymon\JWTAuth\Facades\JWTAuth;


class ProductController extends Controller
{
    /**
     * Default constructor
     */
    public function __construct() {
        $this->middleware('jwt.auth', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = App\Product::all();
        return response()->json($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Nothing
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new App\Product;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->weight = $request->weight;

        if($product->save()) {
            return response()->json([
                'message' => 'Successfully added Product!',
            ], 200);
        } else {
            return response()->json([
                'message' => 'There was an error!',
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = App\Product::find($id);
        return response()->json($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        // Nothing
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = App\Product::find($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->weight = $request->weight;

        if($product->save()) {
            return response()->json([
                'message' => 'Successfully saved Product!',
            ], 200);
        } else {
            return response()->json([
                'message' => 'There was an error!',
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (App\Product::destroy($id) > 0) {
            $message = 'Successfully deleted Product.';
            $code = 200;
        } else {
            $message = 'Internal server error';
            $code = 500;
        }
        return response()->json([
            'message' => $message,
            'code' => $code
        ], $code);
    }
}
