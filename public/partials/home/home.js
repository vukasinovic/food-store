var HomeControllers = angular.module('HomeControllers', []);

HomeControllers.controller('HomeController', ['$scope','$location','HttpGetServices', function($scope, $location,HttpGetServices) {
	$scope.showSpinner = true;
	$scope.goSingleFoodMenu = function (id) {
        $location.path('/single-view/' + id);
    };

    HttpGetServices.getListData('product').then(function(response) {
    	$scope.products = response;
    	$scope.showSpinner = false;
    });

}]);