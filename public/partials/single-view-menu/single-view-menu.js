var SingleViewMenuControllers = angular.module('SingleViewMenuControllers', []);

SingleViewMenuControllers.controller('SingleViewMenuController', ['$scope','$routeParams','HttpGetServices','$location', function($scope,$routeParams,HttpGetServices,$location) {
	$scope.showSpinner = true;
	HttpGetServices.getListData('product/' + $routeParams.id).then(function(response) {
    	$scope.product = response;
		$scope.showSpinner = false;
    	if(!$scope.product.id) {
    		$location.path('/');
    	}
    });
}]);