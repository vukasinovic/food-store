var LoginControllers = angular.module('LoginControllers', []);

LoginControllers.controller('LoginController', ['$scope','$location','HttpPostServices','localStorageService','$rootScope', function($scope, $location, HttpPostServices, localStorageService, $rootScope) {
	$scope.user = {
		email: '',
		password: ''
	};
	$scope.signIn = function () {
		HttpPostServices.login($scope.user).then(function(response) {
	    	console.log(response);
	    	if(response.error){
	    		alert("Doslo je do greske");
	    	}
	    	else{
	    		localStorageService.set('token', response.token);
    		    if(localStorageService.get('token')) {
			        $rootScope.isToken = true;
			    }
			    else {
			        $rootScope.isToken = false;
			    }
				$location.path('/admin-home-menu');
	    	}
	    });
    };
}]); 