var AdminChangeControllers = angular.module('AdminChangeControllers', []);

AdminChangeControllers.controller('AdminChangeController', ['$scope','HttpGetServices','$routeParams','HttpChangeServices','$location', function($scope,HttpGetServices,$routeParams,HttpChangeServices,$location) {
	$scope.showSpinner = true;
	HttpGetServices.getListData('product/' + $routeParams.id).then(function(response) {
    	$scope.product = response;
    	$scope.showSpinner = false;
    });

    $scope.saveChanges = function() {
    	if($scope.product.name != '' & $scope.product.price != '' & $scope.product.weight != '' & $scope.product.description != '') {
	    	HttpChangeServices.changePost('product/' + $routeParams.id, $scope.product).then(function(response) {
	    		alert(response.message);
	    		$location.path('/admin-home-menu');
	    	});
	    }
	    else {
	    	alert('Popunite sva polja!');
	    }
    }

    $scope.goAdminHome = function() {
    	$location.path('/admin-home-menu');
    }
}]);