var AdminAddNewControllers = angular.module('AdminAddNewControllers', []);

AdminAddNewControllers.controller('AdminAddNewController', ['$scope','HttpAddNewServices','$location', function($scope,HttpAddNewServices,$location) {
	$scope.product = {
		name: '',
		price: '',
		weight: '',
		description: ''
	}
	$scope.goAddNew = function() {
		if($scope.product.name != '' & $scope.product.price != '' & $scope.product.weight != '' & $scope.product.description != '') {
	    	HttpAddNewServices.addNewPost('product', $scope.product).then(function(response) {
    			alert(response.message);
    			$location.path('/admin-home-menu');
			});
   		}
   		else {
   			alert('Popunite sva polja!');
   		}
    }
    
    $scope.goAdminHome = function() {
    	$location.path('/admin-home-menu');
    }
}]);