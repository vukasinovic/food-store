var AdminHomeMenuControllers = angular.module('AdminHomeMenuControllers', []);

AdminHomeMenuControllers.controller('AdminHomeMenuController', ['$scope','$location','HttpGetServices','HttpDeleteServices', function($scope, $location, HttpGetServices, HttpDeleteServices) {
	$scope.showSpinner = true;
  HttpGetServices.getListData('product').then(function(response) {
    	$scope.products = response;
      $scope.showSpinner = false;
    });

   	$scope.delete = function(id){
   		HttpDeleteServices.deletePost('product/' + id).then(function(response) {
   			HttpGetServices.getListData('product').then(function(response) {
		    	$scope.products = response;
		    });
   		});
   	}

    $scope.goChangePage = function (id) {
        $location.path('/admin-change/' + id);
    };

    $scope.goAddNew = function () {
        $location.path('/admin-add-new');
    };

}]);