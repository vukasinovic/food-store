var MainControllers = angular.module('MainControllers', []);

MainControllers.controller('MainController', ['$scope','$location','HttpLogoutServices','localStorageService','$rootScope', function($scope, $location,HttpLogoutServices,localStorageService,$rootScope) {
	$scope.goLogin = function () {
        $location.path('/login');
    };
    $scope.goHome = function () {
        $location.path('/');
    };
    $scope.goAdminPanel = function () {
        $location.path('/admin-home-menu');
    };
    $scope.goLogout = function() {
    	HttpLogoutServices.logout();
	    if(localStorageService.get('token')) {
	        $rootScope.isToken = true;
	    }
	    else {
	        $rootScope.isToken = false;
	    }
	}
}]);