var myApp = angular.module('myApp', [
	'ngRoute',
    'angularSpinner',
    'LocalStorageModule',
    'HomeControllers',
    'myAppServices',
    'MainControllers',
    'LoginControllers',
    'SingleViewMenuControllers',
    'AdminHomeMenuControllers',
    'AdminChangeControllers',
    'AdminAddNewControllers'
]);

myApp.config(['$routeProvider','localStorageServiceProvider','usSpinnerConfigProvider','$httpProvider', function($routeProvider,localStorageServiceProvider,usSpinnerConfigProvider,$httpProvider){
	$routeProvider.
	when('/', {
        templateUrl: 'partials/home/home.html',
        controller: 'HomeController'
    }).
    when('/login', {
        templateUrl: 'partials/login/login.html',
        controller: 'LoginController'
    }).
    when('/single-view/:id', {
        templateUrl: 'partials/single-view-menu/single-view-menu.html',
        controller: 'SingleViewMenuController'
    }).
    when('/admin-home-menu', {
        templateUrl: 'partials/admin-home-menu/admin-home-menu.html',
        controller: 'AdminHomeMenuController'
    }).
    when('/admin-change/:id', {
        templateUrl: 'partials/admin-change/admin-change.html',
        controller: 'AdminChangeController'
    }).
    when('/admin-add-new', {
        templateUrl: 'partials/add-new/add-new.html',
        controller: 'AdminAddNewController'
    }).
    otherwise({
        redirectTo: '/'
    });

    localStorageServiceProvider
        .setPrefix('foodstore');

    usSpinnerConfigProvider.setDefaults({color: 'green'});

    $httpProvider.interceptors.push('myHttpResponseInterceptor');
}]);

myApp.run(['localStorageService','$rootScope','$location', function(localStorageService, $rootScope, $location) {
    if(localStorageService.get('token')) {
        $rootScope.isToken = true;
    }
    else {
        $rootScope.isToken = false;
    }

    $rootScope.$on("$locationChangeStart", function(event, next, current) { 
        if($location.path() == '/admin-change' || $location.path() == '/admin-home-menu' || $location.path() == '/admin-add-new') {
            if(!localStorageService.get('token')){
                $location.path('/');
            }
        }

    });
}]);