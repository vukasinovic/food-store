var myAppServices = angular.module('myAppServices', []);
var APIURL = "http://localhost:8000/api/";

myAppServices.service('HttpGetServices', ['$http', function($http) {
    function getListData(partial) {
        return $http.get(APIURL + partial)
            .then(function(response) {
                return response.data;
            })
    }

    return {
    	getListData: getListData
    }
}]);

myAppServices.service('HttpPostServices', ['$http', function($http) {
    function login(data) {
        return $http.post(APIURL + 'authenticate', data)
            .then(function(response) {
                return response.data;
            },
            function(error) {
                return error.data;
            }
            )
    }

    return {
        login: login
    }
}]);

myAppServices.service('HttpDeleteServices', ['$http','localStorageService', function($http,localStorageService) {
    function deletePost(partial) {
        return $http.delete(APIURL + partial, {
            headers: { 'Authorization' : "Bearer " + localStorageService.get('token')
        }
        })
            .then(function(response) {
                return response.data;
            })
    }

    return {
        deletePost: deletePost
    }
}]);

myAppServices.service('HttpChangeServices', ['$http','localStorageService', function($http,localStorageService) {
    function changePost(partial,data) {
        return $http.put(APIURL + partial, data, {
            headers: { 'Authorization' : "Bearer " + localStorageService.get('token')
        }
        })
            .then(function(response) {
                return response.data;
            })
    }

    return {
        changePost: changePost
    }
}]);

myAppServices.service('HttpAddNewServices', ['$http','localStorageService', function($http,localStorageService) {
    function addNewPost(partial,data) {
        return $http.post(APIURL + partial, data, {
            headers: { 'Authorization' : "Bearer " + localStorageService.get('token')
        }
        })
            .then(function(response) {
                return response.data;
            })
    }

    return {
        addNewPost: addNewPost
    }
}]);

myAppServices.service('HttpLogoutServices', ['localStorageService','$location', function(localStorageService,$location) {
    function logout() {
        localStorageService.clearAll();
        $location.path('/');
    }

    return {
        logout: logout
    }
}]);

myAppServices.service('myHttpResponseInterceptor',['$q','$location','localStorageService','$rootScope',function($q,$location,localStorageService,$rootScope){
    var service = this;

    service.responseError = function(response) {
        if (response.status == 401 || response.status == 403){
            alert('Session Expired');
            $location.path('/login');
            localStorageService.clearAll();
        }
        if (response.status == 400){
            alert('Login first');
            localStorageService.clearAll();
            $location.path('/login');
            if(localStorageService.get('token')) {
                $rootScope.isToken = true;
            }
            else {
                $rootScope.isToken = false;
            }
        }
        if (response.status == 500){
            alert('Internal Server Error');
        }

        return $q.reject(response);
    };
}]);


